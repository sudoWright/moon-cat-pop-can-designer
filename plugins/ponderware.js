const LIB_PATH = '/lib/libmooncat.js'

export default (context) => {
  context.$mooncat = {
    loaded: false,
    lib: null,
  }
  ;(function (doc, ctx, script) {
    script = doc.createElement('script')
    script.type = 'text/javascript'
    script.async = true
    script.onload = function () {
      try {
        ctx.$mooncat.lib = window.LibMoonCat
        ctx.$mooncat.loaded = true
        ctx.store.dispatch('mooncat/libLoaded', { loaded: true })
      } catch (err) {}
    }
    script.src = LIB_PATH
    doc.getElementsByTagName('head')[0].appendChild(script)
  })(document, context)
}
