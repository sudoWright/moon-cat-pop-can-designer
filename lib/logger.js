const STORAGE_KEY = 'MCBLOG'

class mcbLog {
  constructor(rescueId) {
    this.trail = []
    this.active = __NUXT__.config.stage === 'dev'
    this.context = ''

    try {
      if (localStorage.getItem(STORAGE_KEY)) {
        this.active = true
      }
    } catch (err) {}
  }

  isActive() {
    return this.active
  }

  active(bool) {
    bool = bool || true
    this.active = bool
  }

  setContext(context) {
    this.context = context
  }

  getTrail() {
    return this.trail
  }

  info(message, ...objects) {
    this._log('info', message, ...objects)
  }

  warning(message, ...objects) {
    this._log('warning', message, ...objects)
  }

  _log(type, message, ...objects) {
    if (this.active)
      objects.length > 0
        ? console.log(
            `[${type.toUpperCase()}] ${this.context}: ${message}`,
            objects
          )
        : console.log(`[${type.toUpperCase()}] ${this.context}: ${message}`)

    // this.trail.push({
    //   type,
    //   context: this.context,
    //   ts: Date.now(),
    //   message,
    //   objects,
    // })
  }
}

const Logger = new mcbLog()
window.mcbLog = Logger

export default Logger
