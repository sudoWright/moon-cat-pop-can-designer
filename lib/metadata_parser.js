let PopContractMetadata = (function () {
  function setUint16(arr, offset, val) {
    let v1 = (val >> 8) & 255
    let v2 = val & 255
    arr[offset] = v1
    arr[offset + 1] = v2
  }

  function getUint16(arr, offset) {
    return (arr[offset] << 8) + arr[offset + 1]
  }

  function bytesToHex(bytes) {
    var hex = '0x'
    for (var i = 0; i < bytes.byteLength; i++) {
      if (bytes[i] < 16) {
        hex += '0' + bytes[i].toString(16)
      } else {
        hex += bytes[i].toString(16)
      }
    }
    return hex
  }

  function hexToBytes(hex) {
    if (hex.slice(0, 2) == '0x') {
      hex = hex.slice(2)
    }
    let bytes = new Uint8Array(hex.length / 2)
    for (let i = 0; i < hex.length / 2; i++) {
      let offset = i * 2
      bytes[i] = parseInt(hex.slice(offset, offset + 2), 16)
    }
    return bytes
  }

  function encodeMetadata(metadata) {
    let textEncoder = new TextEncoder('utf8')
    let flavorBytes = textEncoder.encode(metadata.flavor)
    let totalAccessories = metadata.accessories.length
    var len = 4 + totalAccessories * 5 + 6 + flavorBytes.length

    let bytes = new Uint8Array(len)
    setUint16(bytes, 0, metadata.rescueOrder)
    setUint16(bytes, 2, totalAccessories)
    for (let i = 0; i < totalAccessories; i++) {
      let offset1 = (i + 2) * 2
      let offset2 = offset1 + totalAccessories * 2
      setUint16(bytes, offset1, metadata.accessories[i].id)
      setUint16(bytes, offset2, metadata.accessories[i].zIndex)
    }

    var offset = 4 + totalAccessories * 4
    for (let i = 0; i < totalAccessories; i++) {
      bytes[offset] = metadata.accessories[i].paletteIndex
      offset++
    }
    bytes[offset] = metadata.color
    offset++
    bytes[offset] = metadata.textcolor
    offset++
    bytes[offset] = metadata.textshadow
    offset++
    bytes[offset] = metadata.gradient
    offset++
    bytes[offset] = metadata.pattern
    offset++
    bytes[offset] = metadata.scale
    offset++

    for (var i = 0; i < flavorBytes.length; i++) {
      bytes[offset] = flavorBytes[i]
      offset++
    }
    return bytesToHex(bytes)
  }

  function decodeMetadata(hex) {
    let bytes = hexToBytes(hex)
    let metadata = {
      rescueOrder: getUint16(bytes, 0),
      accessories: [],
    }

    let totalAccessories = getUint16(bytes, 2)
    for (let i = 0; i < totalAccessories; i++) {
      let offset1 = (i + 2) * 2
      let offset2 = offset1 + totalAccessories * 2
      metadata.accessories[i] = {
        id: getUint16(bytes, offset1),
        zIndex: getUint16(bytes, offset2),
      }
    }

    var offset = 4 + totalAccessories * 4
    for (let i = 0; i < totalAccessories; i++) {
      metadata.accessories[i].paletteIndex = bytes[offset]
      offset++
    }

    metadata.color = bytes[offset]
    offset++
    metadata.textcolor = bytes[offset]
    offset++
    metadata.textshadow = bytes[offset]
    offset++
    metadata.gradient = bytes[offset]
    offset++
    metadata.pattern = bytes[offset]
    offset++
    metadata.scale = bytes[offset]
    offset++

    let textDecoder = new TextDecoder('utf8')
    metadata.flavor = textDecoder.decode(bytes.slice(offset))

    return metadata
  }

  function checkEncoder(metadata) {
    let encoded = encodeMetadata(metadata)
    let decoded = decodeMetadata(encoded)
    let keys = Object.keys(metadata).sort()
    if (keys.toString() != Object.keys(decoded).sort().toString()) {
      throw `Mismatched Keys.\nExpected: ${keys.toString()}.\n     Got: ${Object.keys(
        decoded
      )
        .sort()
        .toString()}`
    }
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i]
      if (key != 'accessories' && metadata[key] != decoded[key]) {
        throw `Mismatched value for key: ${key}.\nExpected: ${metadata[key]},\n     Got: ${decoded[key]}`
      }
    }

    if (metadata.accessories.length != decoded.accessories.length) {
      throw `Mismatched accessory lengths.\nExpected: ${metadata.accessories.length},\n     Got: ${decoded.accessories.length}`
      return false
    }

    for (let i = 0; i < metadata.accessories.length; i++) {
      if (metadata.accessories[i].id != decoded.accessories[i].id) {
        throw `Mismatched accessory id at index: ${i}.\nExpected: ${metadata.accessories[i].id},\n     Got: ${decoded.accessories[i].id}`
      }
      if (metadata.accessories[i].zIndex != decoded.accessories[i].zIndex) {
        throw `Mismatched accessory zIndex at index: ${i}.\nExpected: ${metadata.accessories[i].zIndex},\n     Got: ${decoded.accessories[i].zIndex}`
      }
      if (
        metadata.accessories[i].paletteIndex !=
        decoded.accessories[i].paletteIndex
      ) {
        throw `Mismatched accessory paletteIndex at index: ${i}.\nExpected: ${metadata.accessories[i].paletteIndex},\n     Got: ${decoded.accessories[i].paletteIndex}`
      }
    }
    return true
  }

  let sample = {
    rescueOrder: 22,
    accessories: [
      { id: 0, zIndex: 2, paletteIndex: 0 },
      { id: 948, zIndex: 1, paletteIndex: 0 },
    ],
    color: 20,
    textcolor: 30,
    textshadow: 0,
    gradient: 1,
    pattern: 2,
    scale: 0,
    flavor: 'Delicious Flavor ☺',
  }

  return {
    encode: encodeMetadata,
    decode: decodeMetadata,
    check: checkEncoder,
    sample,
  }
})()

if (typeof module === 'object') {
  module.exports = PopContractMetadata
}
