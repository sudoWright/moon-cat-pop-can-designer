const fs = require('fs')
const winners = require('./exports/winners_export_processed.json')

let sortedWinners = winners.sort((a, b) => a.mint - b.mint)
let output = []
const EXPORT_PATH = './exports/'

for (let i in sortedWinners) {
  let vm = sortedWinners[i]
  let parts = vm.texture.split('?')
  let params = parts[1].split('&').map((param) => {
    let p = param.split('=')
    return p
  })
  let paramObj = {}
  for (let p in params) {
    paramObj[params[p][0]] = params[p][1]
  }
  console.log(paramObj)

  let acc = []

  if (paramObj.accessories !== '0') {
    acc = paramObj.accessories
      .split(',')
      .map((a) => Number(a))
      .reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / 3)

        if (!resultArray[chunkIndex]) {
          resultArray[chunkIndex] = [] // start a new chunk
        }

        resultArray[chunkIndex].push(item)

        return resultArray
      }, [])
  }
  output.push([
    [
      vm.rescueOrder,
      Number(paramObj.color),
      Number(paramObj.textcolor),
      Number(paramObj.textshadow),
      Number(paramObj.scale),
      Number(paramObj.gradient),
      Number(paramObj.pattern),
      vm.flavor,
    ],
    12800,
    acc,
  ])
}

fs.writeFile(EXPORT_PATH + `mints.json`, JSON.stringify(output), (err) => {
  if (err) {
    console.error(err)
    return
  }
})
