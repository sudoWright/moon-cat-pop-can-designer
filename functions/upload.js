const firebaseAdmin = require('firebase-admin');
const { v4: uuidv4 } = require('uuid');

const serviceAccount = require('./mooncat-can-designer-firebase-adminsdk-dlkc5-456a078020.json')

const admin = firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
});

const storageRef = admin.storage().bucket(`gs://pop-assets.mooncat.community`);

async function uploadFile(path, filename) {
  const storage = await storageRef.upload(path, {
      public: true,
      destination: `can/${filename}`, // vm/
      metadata: {
          firebaseStorageDownloadTokens: uuidv4(),
      }
  });
  return storage[0].metadata.mediaLink;
}

(async() => {
  for(let i = 0; i <= 83; i++) {
    // const url = await uploadFile(`../bin/renders/vm_${i}.mp4`, `vm_${i}.mp4`);
    const url = await uploadFile(`../bin/renders/${i}.mp4`, `${i}.mp4`);
    console.log(url);
  }
})();