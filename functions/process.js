const fs = require('fs')
const winners = require('./exports/winners_export.json')

let tickets = new Map()

for (let i in winners) {
  tickets.set(winners[i].rescueOrder, winners[i])
}

console.log(tickets.size)
console.log(
  winners.map((ticket) =>
    typeof ticket.rescueOrder !== 'undefined' ? ticket.rescueOrder : '?'
  )
)

let mintOrder = [
  0, 2723, 10484, 3294, 1756, 12693, 1337, 2165, 1039, 476, 217, 1117, 15401,
  5766, 20451, 24673, 2726, 24704, 527, 39, 18074, 1289, 1069, 6939, 14949, 6,
  9, 19507, 41, 1514, 1218, 13358, 23677, 2665, 75, 3138, 24234, 1070, 10,
  18189, 13349, 1264, 302, 19604, 17970, 453, 3632, 5670, 9665, 283, 1382, 6755,
  13872, 19373, 20800, 403, 3204, 86, 1684, 12, 22313, 22, 3137, 8125, 899,
  7500, 13675, 14737, 419, 24335, 1829, 13097, 2710, 7265, 526, 19533, 635,
  6612, 3028, 2807, 4723, 15508, 11120, 11676,
]

for (let mint in mintOrder) {
  if (tickets.has(mintOrder[mint])) {
    let ticket = tickets.get(mintOrder[mint])
    ticket.mint = Number(mint)
    tickets.set(mintOrder[mint], ticket)
  }
}

fs.writeFile(
  './exports/winners_export_processed.json',
  JSON.stringify(
    Array.from(tickets)
      .map((ticket) => ticket[1])
      .sort((a, b) => a.mint - b.mint)
  ),
  (err) => {
    if (err) {
      console.error(err)
      return
    }
    //file written successfully
  }
)
