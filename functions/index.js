const fs = require('fs')
const functions = require('firebase-functions')
const admin = require('firebase-admin')
const Tickets = require('./lib/tickets')

const app = admin.initializeApp({
  projectId: 'mooncat-can-designer',
  credential: admin.credential.applicationDefault(),
})

const db = app.firestore()
const timestamp = admin.firestore.Timestamp

const inEmulator =
  typeof process.env.FUNCTIONS_EMULATOR !== 'undefined' &&
  process.env.FUNCTIONS_EMULATOR

const ticketsCollection = db.collection(inEmulator ? 'dev-tickets' : 'tickets')
const machinesCollection = db.collection(inEmulator ? 'dev-machines' : 'machines')

exports.storeTicket = functions.https.onCall(async (data, context) => {
  try {
    const docRef = ticketsCollection.doc(String(data.metadata.rescueOrder))
    const doc = await docRef.get()

    data.created = timestamp.now()

    if (doc.exists) {
      await docRef.update(data)
    } else {
      await docRef.set(data)
    }

    return {
      id: doc.id,
    }
  } catch (err) {
    return err
  }
})

exports.updateSupply = functions.https.onCall(async (data, context) => {
  try {
    const docRef = machinesCollection.doc(String(data.id))
    const doc = await docRef.get()

    if (doc.exists) {
      await docRef.update({supply: Number(data.supply)}) // @dev - consider only updating if not updated in last x mins
    }

    return
  } catch (err) {
    return err
  }
})

exports.updateDescription = functions.https.onCall(async (data, context) => {
  try {
    const docRef = machinesCollection.doc(String(data.id))
    const doc = await docRef.get()

    if (doc.exists) {
      await docRef.update({description: String(data.description)})
    }

    return
  } catch (err) {
    return err
  }
})

exports.winningTickets = functions
  .runWith({
    // Ensure the function has enough memory and time
    // to process large files
    timeoutSeconds: 300,
    // memory: '1GB',
  })
  .https.onCall(async (data, context) => {
    try {
      let docRef = db.collection('tickets')
      let snapshot = await docRef.get()

      let ticketMap = new Map()
      let missing = []
      let mismatch = []

      while (snapshot.docs.length) {
        let doc = snapshot.docs.shift()
        let data = doc.data()

        let ticketId =
          typeof data.ticket === 'object'
            ? parseInt(String(data.ticket._hex), 16)
            : data.ticket
        data.ticketId = ticketId

        ticketMap.set(String(ticketId), data)
      }

      let winners = new Tickets()
      let json = await winners.loadWinners(data)

      for (let i in json) {
        if (!ticketMap.has(i)) missing.push(json[i])
        // else if (json[i].address.toLowerCase() !== ticketMap.get(String(i))['address'].toLowerCase())
        //   mismatch.push(json[i])
        else {
          let ticketData = ticketMap.get(String(i))
          let name = await winners.getName(ticketData.metadata.rescueOrder)

          json[i] = {
            ...json[i],
            rescueOrder: ticketData.metadata.rescueOrder,
            flavor: ticketData.metadata.flavor,
            tabColor: winners.getTabColor(ticketData.metadata.rescueOrder),
            texture: winners.getTextureUrl(ticketData.metadata),
            discord: ticketData.discord,
            traits: winners.getTraits(ticketData.metadata.rescueOrder),
            name: name,
          }
        }
      }

      console.log('missing', missing)
      console.log('mismatch', mismatch)

      //   let docRef = db
      //     .collection('tickets')
      //     .where('ticket', '==', json[i].ticket)
      //   let snapshot = await docRef.get()

      //   while (snapshot.docs.length) {
      //     let doc = snapshot.docs.shift()
      //     let id = doc.id
      //     let data = doc.data()

      //     json[i] = {
      //       ...json[i],
      //       rescueOrder: data.metadata.rescueOrder,
      //       flavor: data.metadata.flavor,
      //       tabColor: winners.getTabColor(data.metadata.rescueOrder),
      //       texture: winners.getTextureUrl(data.metadata),
      //       traits: winners.getTraits(data.metadata.rescueOrder),
      //     }
      //   }
      // }

      fs.writeFile('./exports/winners_export.json', JSON.stringify(json), (err) => {
        if (err) {
          console.error(err)
          return
        }
        //file written successfully
      })

      return 'Done'
    } catch (err) {
      return err
    }
  })
