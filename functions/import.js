const fs = require('fs')
const { initializeApp, restore } = require('firestore-export-import')
const serviceAccount = require('./mooncat-can-designer-firebase-adminsdk-dlkc5-456a078020.json')
const winners = require('./exports/winners_export_processed.json')

const EXPORT_PATH = './exports/'

const appName = '[DEFAULT]'
initializeApp(serviceAccount, appName)

let sortedWinners = winners.sort((a, b) => b.mint - a.mint)
let json = {
  machines: {},
}

for (let i in sortedWinners) {
  let vm = sortedWinners[i]

  let vmData = {
    active: true,
    flavor: vm.flavor,
    mint: Number(vm.mint),
    rescueOrder: Number(vm.rescueOrder),
    supply: 100,
  }

  if (typeof vm.name !== 'undefined' && vm.name !== null) vmData.name = vm.name

  json.machines[String(vm.mint)] = vmData
}

fs.writeFile(EXPORT_PATH + 'machines.json', JSON.stringify(json), (err) => {
  if (err) {
    console.error(err)
    return
  }
})

// restore(EXPORT_PATH + 'machines.json')
