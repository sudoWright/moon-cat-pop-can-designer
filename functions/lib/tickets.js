const { ethers } = require('ethers')
const functions = require('firebase-functions')
const fetch = require('node-fetch')
const LibMoonCat = require('./libmooncat')
const raffleABI = require('./abi/raffle.json')

const RAFFLE_ADDR = '0x4a859aD2E94004dbD65Ad75B26719F289370083F'
const INFURA_PROJECTID = functions.config().candesigner.infuraid
const API_URL = 'https://mcpt.mooncat.community'

class Tickets {
  constructor() {
    this.winners = []
    this.json = []
    this.provider = new ethers.providers.InfuraProvider(
      'mainnet',
      INFURA_PROJECTID
    )
    this.contract = new ethers.Contract(RAFFLE_ADDR, raffleABI, this.provider)
  }

  async loadWinners(data) {
    try {
      let ticketsIssued = await this.contract.ticketsIssued()
      let returnAll = typeof data.all !== 'undefined' && data.all === true

      for (let i = 0; i < ethers.BigNumber.from(ticketsIssued).toNumber(); i++)
        if (returnAll || (await this.contract.isWinner(i))) this.winners.push(i)
    } catch (err) {
      // Likely raffle is currently still open, use test winners instead.
      this.winners.push(3)
      this.winners.push(6)
      this.winners.push(8)
    }

    for (let ticket in this.winners) {
      this.json.push({
        ticket: this.winners[ticket],
        address: await this.contract.holderByTicketId(this.winners[ticket]),
      })
    }
    this.json.push({
      ticket: 78,
      address: '0xeDccC2Ce220f286bF218390Ad16E432D539E6890',
    })
    this.json.push({
      ticket: 79,
      address: '0x599b6061524d4dc92edffed096de023576517e1d',
    })
    this.json.push({
      ticket: 80,
      address: '0x288F3683ca38a9945Cb4F76EE91f313Da2D31657',
    })
    this.json.push({
      ticket: 81,
      address: '0x89546B6DdE5176C1FF820D210e532D928BF69465',
    })
    this.json.push({
      ticket: 82,
      address: '0xB6fFCA576057f37DDFA2DD6159D56519a4Ec5F21',
    })
    this.json.push({
      ticket: 83,
      address: '0x384973Ce79c352aD766F56672BABec85e897f234',
    })

    return this.json
  }

  getTextureUrl(data) {
    let accessories = [
      ...data.accessories.map((acc) => [acc.id, acc.paletteIndex, acc.zIndex]),
    ]
      .flat()
      .join(',')

    if (accessories.length === 0) accessories = 0

    return `${API_URL}/pop-texture?id=${
      data.rescueOrder
    }&accessories=${accessories}&color=${data.color}&textcolor=${
      data.textcolor
    }&textshadow=${data.textshadow}&scale=${
      data.scale
    }&flavor=${encodeURIComponent(data.flavor)}&gradient=${
      data.gradient
    }&pattern=${data.pattern}&full=true`
  }

  getTabColor(rescueOrder) {
    let catId = LibMoonCat.getCatId(rescueOrder)
    let fullPalette = LibMoonCat.fullPalette(catId)
    let coat = fullPalette.filter((color) => color.id === 116)
    return this._rgbToHex(coat[0].rgba)
  }

  getTraits(rescueOrder) {
    let catId = LibMoonCat.getCatId(rescueOrder)
    return LibMoonCat.getTraits('extended', catId)
  }

  async getName(rescueOrder) {
    try {
      let response = await fetch(
        `https://api.mooncat.community/traits/${rescueOrder}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      const json = await response.json()
      return json.details.name
    } catch (err) {
      console.log(`Error getting MoonCat Details for #${rescueOrder}`, err)
      return false
    }
  }

  _rgbToHex(rgbArr) {
    return (
      '0x' +
      ((1 << 24) + (rgbArr[0] << 16) + (rgbArr[1] << 8) + rgbArr[2])
        .toString(16)
        .slice(1)
    )
  }
}

module.exports = Tickets
