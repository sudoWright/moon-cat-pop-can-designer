export default {
  data() {
    return {
      transaction: {
        processing: false,
        success: false,
        error: {
          state: false,
          message: null,
        },
      },
    }
  },
  methods: {
    resetTransaction() {
      this.transaction.processing = false
      this.transaction.success = false
      this.transaction.error.state = false
      this.transaction.error.message = null
    },
    completeTransaction() {
      this.$router.push('/')
    },
    async processingTransaction(tx) {
      console.log('processing tx')
      if (typeof tx.wait === 'function') {
        const receipt = await tx.wait()
        // @dev - add receipt to Logger
        // console.log(receipt, 'minted')
        this.transaction.success = true
        this.$store.dispatch('basket/clearBasket')
        this.$store.dispatch('identity/txOutcome', true)
        this.$store.dispatch('identity/txRecord', tx)
        return true
      } else {
        // @dev - add err to Logger
        if (typeof tx.error !== 'undefined') {
          this.transaction.error.message = tx.error.message
        } else if (
          typeof tx.data !== 'undefined' &&
          typeof tx.data.message !== 'undefined'
        )
          this.transaction.error.message = tx.data.message.replace(
            'Error: VM Exception while processing transaction: ',
            ''
          )
        else if (typeof tx.message !== 'undefined')
          this.transaction.error.message = tx.message
        this.transaction.error.state = true
        this.$store.dispatch('identity/txOutcome', false)
        this.$store.dispatch(
          'identity/txRecord',
          this.transaction.error.message
        )
        return false
      }
    },
  },
}
