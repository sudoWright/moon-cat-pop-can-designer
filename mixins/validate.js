const validate = {
  data() {
    return {
      vd: {
        hasChanges: false,
        changes: {},
        hasErrors: false, // managed via Watch
        errors: [],
        global: {
          error: null,
        },
      },

      // example field item
      // [key]: {
      //   valid: false, // input is valid
      //   dirty: false, // input has been intereacted with
      //   error: false, // used to display errors if !valid && dirty || error message or null
      //   model: null,
      //   prop: null
      //   ...validations (required...)
      // }

      // object in comp using validate mixin
      // form: {
      //   data: {}, // optional, used for prop style data setting
      //   validations: [] // required
      // }
    }
  },

  directives: {
    vdForm: {
      inserted(el, _, vnode) {
        el.setAttribute('novalidate', true)
      },
    },

    vdInput: {
      inserted(el, _, vnode) {
        const vm = vnode.context
        vm.initVdField(el)
      },

      bind(el, _, vnode) {
        const vm = vnode.context

        // listen to input changes
        el.addEventListener('input', (e) => vm.vdInputChange(el))

        // listen to input blurs
        el.addEventListener('blur', (e) => vm.vdInputBlur(el))

        vnode.context.$on('vdValidate', () => {
          vm.vdValidateField(el)
        })

        vnode.context.$on('vdTouch', () => {
          vm.vdSetFieldDirty(el.name)
        })
      },

      unbind(el, _, vnode) {
        const vm = vnode.context

        // remove listener for input changes
        el.removeEventListener('input', vm.vdInputChange(el))

        // remove listener for input blurs
        el.removeEventListener('blur', vm.vdInputChange(el))
      },
    },
  },

  watch: {},

  computed: {},

  created() {
    this.initVdFields()
  },

  methods: {
    initVdFields() {
      if (typeof this.form.validations !== 'undefined') {
        this.form.validations.forEach((key) => {
          // get defaults
          const defaults = this._defaultFieldOptions()

          // get field data or set null if empty i.e. new form
          const fieldData = this._setFieldPropData(key)

          // merge defaults, validations and fieldData
          const field = Object.assign(defaults, fieldData)

          this.$set(this.vd, key, field)
        })
      }
    },

    initVdField(input) {
      const key = input.name

      // setup field defaults
      const defaults = this._defaultFieldOptions()

      // [validations - required (valueMissing), email(typeMismatch)]
      const validations = this._setFieldValidations(input)

      // set field data incase their are props
      const fieldData = this._setFieldPropData(key)

      // merge defaults and fieldData
      const field = Object.assign(defaults, validations, fieldData)

      this.$set(this.vd, key, field)
    },

    vdInputChange(el) {
      const key = el.name
      const item = this.vd[key]

      if (item.dirty && !item.valid) {
        this.vdValidateField(el)
      }

      this.vdManageChanges(key)
    },

    vdInputBlur(el) {
      this.vdSetFieldDirty(el.name)
      this.vdValidateField(el)
      this.fetchPreview()
    },

    vdTouch() {
      // set each field dirty true
      this.$emit('vdTouch')

      // trigger a validation check on each field??
      this.$emit('vdValidate')
    },

    vdSetFieldDirty(key) {
      if (typeof this.vd[key] !== 'undefined') {
        this.vd[key].dirty = true
      }
    },

    vdValidateField(el) {
      const item = this.vd[el.name]

      if (typeof item !== 'undefined') {
        const hadError = item.error

        item.valid = el.validity.valid

        if (item.dirty) {
          item.error = !el.validity.valid

          // set validation message to default browser one
          item.errorMessage = item.error ? el.validationMessage : null
        }

        // if field required validate with validity.valueMissing
        if (typeof item.errRequired !== 'undefined') {
          item.errRequired = el.validity.valueMissing
        }

        // if field with formating i.e. email validate with validity.typeMismatch
        if (typeof item.errType !== 'undefined') {
          item.errType = el.validity.typeMismatch
        }

        // update errors array
        this.vdUpdateErrors(hadError, item.error, el.name)
      }
    },

    vdUpdateErrors(hadError, isError, key) {
      // if new error add to array
      if (!hadError && isError) {
        this.vd.errors.push(key)
      }

      // if was error but valid now
      if (hadError && !isError) {
        const index = this.vd.errors.indexOf(key)

        if (index >= 0) {
          this.vd.errors.splice(index, 1)
        }
      }

      this.vd.hasErrors = this.vd.errors.length > 0
    },

    vdForceError(key, message) {
      if (typeof this.vd[key] !== 'undefined') {
        this.vd[key].dirty = true
        this.vd[key].error = true

        if (message !== 'undefined') {
          this.vd[key].errorMessage = message
        }

        this.vd.errors.push(key)
      }
    },

    vdForceGlobalError(message) {
      if (message !== 'undefined') {
        this.vd.global.error = true
        this.vd.global.errorMessage = message

        this.vd.errors.push('global')
      }
    },

    vdManageChanges(key) {
      const changeTracked = typeof this.vd.changes[key] !== 'undefined'
      const after = this.vd[key].model
      let before = ''

      if (
        typeof this.form.data !== 'undefined' &&
        typeof this.form.data[key] !== 'undefined'
      ) {
        before = this.form.data[key]
      }

      // if change not tracked then track it
      if (before !== after && !changeTracked) {
        this.$set(this.vd.changes, key, true)
        this.vd.hasChanges = true
      }

      // if not change but is tracked the delete it
      if (before === after && changeTracked) {
        delete this.vd.changes[key]

        this.vd.hasChanges = Object.keys(this.vd.changes).length !== 0
      }

      this.form.data[key] = after
    },

    vdResetChanges() {
      this.vd.changes = {}
      this.vd.hasChanges = false
    },

    vdReset() {
      this.vdResetChanges()

      this.vd.errors = []
      this.vd.hasErrors = false

      this.vd.global = {
        error: false,
        errorMessage: null,
      }
    },

    vdSetHasChanges(key) {
      this.$set(this.vd.changes, key, true)
      this.vd.hasChanges = true
    },

    _setFieldValidations(inputEl) {
      const validations = {}

      if (inputEl.required) {
        validations.errRequired = false
      }

      if (inputEl.type === 'email') {
        validations.errType = false
      }

      return validations
    },

    _setFieldPropData(field) {
      // set data
      const val =
        typeof this.form.data !== 'undefined' &&
        typeof this.form.data[field] !== 'undefined' &&
        this.form.data[field].length > 0
          ? this.form.data[field]
          : null

      return {
        model: val,
        prop: val,
      }
    },

    _defaultFieldOptions() {
      return {
        valid: false,
        dirty: false,
        error: false,
        errorMessage: null,
      }
    },
  },
}

export default validate
