const BASE_URL = 'https://pop.mooncat.community'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'MoonCatPop',
    description: 'Grab a selection of refreshments to keep your MoonCat full of fizz in the metaverse.',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/styles.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/identity', '~/plugins/storage', '~/plugins/ponderware'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // transpile: ['vue-click-outside'],
  },

  // Runtime Config
  publicRuntimeConfig: {
    baseURL:
      process.env.NODE_ENV === 'production'
        ? BASE_URL
        : 'http://mccd.test:3000',
    stage: process.env.npm_config_stage === 'prod' ? 'prod' : 'dev',
    infuraId: 'e5a5b8a6b9034b2da2afb25755c02d0a',
    contractAddress:
      process.env.npm_config_stage === 'prod'
        ? '0x4a859aD2E94004dbD65Ad75B26719F289370083F'
        : '0x70e0bA845a1A0F2DA3359C97E0285013525FFC49',
    factoryAddress:
      process.env.npm_config_stage === 'prod'
        ? '0x09C61c41C8C5D378CAd80523044C065648Eaa654'
        : '0x99bbA657f2BbC93c02D617f8bA121cB8Fc104Acf',
    vendingAddress:
      process.env.npm_config_stage === 'prod'
        ? '0xb8C11BEda7142ae7986726247f548Eb0C3CDE474'
        : '0x5eb3Bc0a489C5A8288765d2336659EbCA68FCd00',
    apiURL:
      process.env.npm_config_stage === 'prod'
        ? 'https://mcpt.mooncat.community'
        : 'http://localhost:5050',
  },
}
