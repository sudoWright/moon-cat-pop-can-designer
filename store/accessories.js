import mcbLog from '~/lib/logger'
import mcbDeepSpace from '~/lib/deepspace'
import mcbAccessory from '~/lib/accessory'

const dsn = new mcbDeepSpace()

const STORAGE_KEY = 'MCBACCESSORIES'

mcbLog.setContext('store/accessories')

export const state = () => ({
  accessories: {},
})

export const getters = {
  /**
   * Returns an accessory object either from cache or via DeepSpace
   *
   * @param {number} id of the accessory.
   * @return {mcbAccessory} Promise of a new mcbAccessory instance.
   * @throws {boolean} false is returned should an error occur.
   */
  getAccessory: (state) => (id) => {
    return new Promise((resolve, reject) => {
      if (typeof state.accessories[id] !== 'undefined') {
        let accessory = mcbAccessory.fromCache(id, state.accessories[id].cache)
        accessory.setManagerName(state.accessories[id].managerName)
        mcbLog.info(`Got accessory #${id} from local store`)
        resolve(accessory)
      } else {
        mcbLog.info(
          `Accessory #${id} not in local store, loading from Deep Space`
        )
        dsn
          .getAccessory(id)
          .then((result) => {
            mcbLog.info(`Got accessory #${id} from Deep Space`)
            resolve(result)
          })
          .catch((err) => {
            resolve(false)
          })
      }
    })
  },
}

export const mutations = {
  addAccessory(state, accessory) {
    if (!(accessory.getId() in state.accessories)) {
      state.accessories = {
        ...state.accessories,
        [accessory.getId()]: accessory.toObject(),
      }
      mcbLog.info(`Add accessory #${accessory.getId()} to local store`)
    }
  },
  addAccessories(state, accessories) {
    for (let i in accessories) {
      state.accessories = {
        ...state.accessories,
        [accessories[i].getId()]: accessories[i].toObject(),
      }
    }
    mcbLog.info(`Add accessories to local store`, accessories)
  },
}

export const actions = {
  addAccessory({ commit }, accessory) {
    return new Promise((resolve, reject) => {
      commit('addAccessory', accessory)
      resolve()
    })
  },
  addAccessories({ commit }, accessories) {
    return new Promise((resolve, reject) => {
      commit('addAccessories', accessories)
      resolve()
    })
  },
}
