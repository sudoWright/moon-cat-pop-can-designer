import mcbLog from '~/lib/logger'

const STORAGE_KEY = 'MCPBASKET'

mcbLog.setContext('store/basket')

export const state = () => ({
  items: {},
})

export const getters = {
  hasItems(state) {
    return Object.keys(state.items).length > 0
  },
  getItems(state) {
    return Object.values(state.items)
  },
  count(state) {
    return Object.keys(state.items).length
  },
}

export const mutations = {
  initialiseBasket(state) {
    try {
      // Load basket from local storage if present
      if (localStorage.getItem(STORAGE_KEY)) {
        state.items = { ...JSON.parse(localStorage.getItem(STORAGE_KEY)) }
      }
    } catch (err) {}
  },
  clearBasket(state) {
    state.items = { ...{} }
    if (localStorage.getItem(STORAGE_KEY))
      localStorage.setItem(STORAGE_KEY, JSON.stringify(state.items))

    mcbLog.info('Cleared the basket')
  },
  addItem(state, item) {
    if (typeof state.items[item.id] !== 'undefined') {
      state.items[item.id].total += item.total
    } else {
      let items = { ...state.items }
      items[item.id] = {
        ...item,
        order: Object.keys(items).length,
      }
      state.items = { ...items }
    }
  },
  removeItem(state, item) {
    let items = { ...state.items }
    delete items[item.id]
    state.items = { ...items }
  },
  oneMoreItem(state, item) {
    if (typeof state.items[item.id] !== 'undefined') {
      state.items[item.id].total += 1
    }
  },
  oneLessItem(state, item) {
    if (typeof state.items[item.id] !== 'undefined') {
      if (state.items[item.id].total <= 1) {
        let items = { ...state.items }
        delete items[item.id]
        state.items = { ...items }
      } else state.items[item.id].total -= 1
    }
  },
}

export const actions = {
  clearBasket({ commit }) {
    return new Promise((resolve, reject) => {
      commit('clearBasket')
      resolve()
    })
  },
  addItem({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('addItem', payload)
      resolve()
    })
  },
  removeItem({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('removeItem', payload)
      resolve()
    })
  },
  oneMoreItem({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('oneMoreItem', payload)
      resolve()
    })
  },
  oneLessItem({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('oneLessItem', payload)
      resolve()
    })
  },
}
